﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour {
    public GameObject creditsCamera;
    public int speed = 1;
    public float waitTime = 20;
    public int lvlToLoad;
    public AudioClip endSong;
    public AudioSource audio;
    public float timer = 24;


	// Use this for initialization
	void Start () {
        audio.PlayOneShot(endSong);
    }
	
	// Update is called once per frame
	void Update () {
		creditsCamera.transform.Translate(Vector2.down * Time.deltaTime * speed);

        timer -= Time.deltaTime;
        if (timer<= 0 )
        {
            Application.LoadLevel(1);
        }	
	}
    
}
