﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeatDisplay : MonoBehaviour, SongManager.SongNotification {
	public GameObject BeatLinePrefab;
	public GameObject BeatStartPoint;
	public GameObject BeatEndPoint;

	public List<GameObject> BeatLines = new List<GameObject>();
	public float TimeToMoveFullRange = 5.0f;
	private float DistancePerTime;

	private float DistanceToMove;

	public bool MovingUp_Internal = true;
    public BeatDisplay otherBeat;

	private Vector3 StartPoint;
	private Vector3 DestinationPoint;

	public bool MovingUp {
		get {
			return MovingUp_Internal;
		}
		set {
			MovingUp_Internal = value;

			StartPoint = MovingUp_Internal ? BeatStartPoint.transform.position : BeatEndPoint.transform.position;
			DestinationPoint = MovingUp_Internal ? BeatEndPoint.transform.position : BeatStartPoint.transform.position;
		}
	}

	// Use this for initialization
	void Start () {
		SongManager.RegisterSongNotification(this);

		DistanceToMove = (BeatStartPoint.transform.position - BeatEndPoint.transform.position).magnitude;
		DistancePerTime = DistanceToMove / TimeToMoveFullRange;

		StartPoint = MovingUp ? BeatStartPoint.transform.position : BeatEndPoint.transform.position;
		DestinationPoint = MovingUp ? BeatEndPoint.transform.position : BeatStartPoint.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		List<GameObject> beatLinesToDestroy = new List<GameObject>();

		// Move all of the beat lines up
		foreach(GameObject beatLine in BeatLines) {
			beatLine.transform.localPosition = Vector3.MoveTowards(beatLine.transform.localPosition, 
																   DestinationPoint, 
																   DistancePerTime * Time.deltaTime);

            if ((beatLine.transform.localPosition - DestinationPoint).sqrMagnitude < 0.1f)
            {
                beatLinesToDestroy.Add(beatLine);
            }
        }

        // Purge any beatlines that need to be
        if (!MovingUp)
        {
            foreach (GameObject beatLine in beatLinesToDestroy)
            {
                BeatLines.Remove(beatLine);
                GameObject.Destroy(beatLine);
            }
        }
        else
        {
            foreach (GameObject beatLine in beatLinesToDestroy)
            {
				BeatLine beatLineScript = beatLine.GetComponent<BeatLine>();

                BeatLines.Remove(beatLine);
                
                if (beatLine.transform.childCount == 0)
                {
                    GameObject.Destroy(beatLine);

					if (beatLineScript.beatStyle == BeatStyle.Mandatory) {
						if (!GameManager.Instance.upFalseDownTrue)
						{
							GameManager.Instance.player1Score -= 5;
						}
						else if (GameManager.Instance.upFalseDownTrue)
						{
							GameManager.Instance.player2Score -= 5;
						}
					}
                }
                else
                {
                    otherBeat.BeatLines.Add(beatLine);
                    beatLine.transform.SetParent(otherBeat.transform);
                    beatLine.transform.localPosition = otherBeat.StartPoint;
                    if (!GameManager.Instance.upFalseDownTrue)
                    {
                        if (beatLine.transform.GetChild(0).CompareTag("Button0"))
                        {
                            beatLine.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2ButtonSprites[0];
                        }
                        else if (beatLine.transform.GetChild(0).CompareTag("Button1"))
                        {
                            beatLine.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2ButtonSprites[1];
                        }
                        else if (beatLine.transform.GetChild(0).CompareTag("Button2"))
                        {
                            beatLine.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2ButtonSprites[2];
                        }
                        else if (beatLine.transform.GetChild(0).CompareTag("Button3"))
                        {
                            beatLine.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2ButtonSprites[3];
                        }
                    }

                    else if(GameManager.Instance.upFalseDownTrue)
                    {
                        if (beatLine.transform.GetChild(0).CompareTag("Button0"))
                        {
                            beatLine.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1ButtonSprites[0];
                        }
                        else if (beatLine.transform.GetChild(0).CompareTag("Button1"))
                        {
                            beatLine.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1ButtonSprites[1];
                        }
                        else if (beatLine.transform.GetChild(0).CompareTag("Button2"))
                        {
                            beatLine.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1ButtonSprites[2];
                        }
                        else if (beatLine.transform.GetChild(0).CompareTag("Button3"))
                        {
                            beatLine.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1ButtonSprites[3];
                        }
                    }
                }
            }
        }
	}

	public void OnCountingDownToFirstSong(float timeUntilStart) {
	}

	public void OnBeginSong(float songDuration) {
	}

	public void OnEndSong(float timeUntilNextSongStarts) {
	}

	public void OnSwitchPlayer() {
	}

	public void OnSongBeat(int beatIndex, double beatTime, BeatStyle beatStyle) {
		if (MovingUp) {
			StartCoroutine(SpawnBeatLine(beatIndex, beatTime, 0.0f, beatStyle));
		}
		else {
			//StartCoroutine(SpawnBeatLine(beatIndex, beatTime, TimeToMoveFullRange));
		}
	}

	IEnumerator SpawnBeatLine(int beatIndex, double beatTime, float delayTime, BeatStyle beatStyle)
	{
		if (delayTime > 0.0f) {
			yield return new WaitForSeconds(delayTime);
		}

		// Spawn the beat line
		GameObject beatLine = Instantiate(BeatLinePrefab);
		beatLine.transform.SetParent(gameObject.transform);
		beatLine.transform.localPosition = StartPoint;

		// Store the info for the beat line
		BeatLine beatLineScript = beatLine.GetComponent<BeatLine>();
		beatLineScript.BeatIndex = beatIndex;
		beatLineScript.BeatTime = (float) beatTime;
		beatLineScript.beatStyle = beatStyle;

		BeatLines.Add(beatLine);
	}
}
