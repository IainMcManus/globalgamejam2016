﻿using UnityEngine;
using System.Collections;

public class BeatLine : MonoBehaviour {
	public int BeatIndex;
	public float BeatTime;
	public BeatStyle beatStyle = BeatStyle.Mandatory;
    bool enterTrigger1 = false;
    bool enterTrigger2 = false;
	private SpriteRenderer BeatSpriteRenderer;

	// Use this for initialization
	void Start () {
		BeatSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		BeatSpriteRenderer.color = Color.clear;
	}
	
	// Update is called once per frame
	void Update () {
		if (beatStyle == BeatStyle.Mandatory) {
			BeatSpriteRenderer.color = Color.white;
		}
		else if (beatStyle == BeatStyle.Optional_Red) {
			BeatSpriteRenderer.color = Color.red;

			return;
		}
		else if (beatStyle == BeatStyle.Optional_Green) {
			BeatSpriteRenderer.color = Color.green;

			return;
		}
		else if (beatStyle == BeatStyle.Optional_Orange) {
			BeatSpriteRenderer.color = Color.yellow;

			return;
		}

        if (!GameManager.Instance.upFalseDownTrue)
        {
            if (enterTrigger1 && this.gameObject.transform.childCount < 1)
            {
                if (Input.GetKeyDown(KeyCode.A))
                {
                    GameObject newBeat = Instantiate(GameManager.Instance.Player1.GetComponent<PlayerScript>().button[0], GameManager.Instance.Player1.GetComponent<PlayerScript>().buttonStart[0].transform.position, GameManager.Instance.Player1.GetComponent<PlayerScript>().buttonStart[0].transform.rotation) as GameObject;
                    newBeat.transform.SetParent(this.gameObject.transform);
                    newBeat.transform.localPosition = new Vector3(-1f, 0, 0);
                    GameManager.Instance.player1HitBeats++;
                    GameManager.Instance.player1Score += 5;
                    GameManager.Instance.Player1SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1Sprites[0];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.W)) //This is not an else so that it can record more than one hit
                {
                    GameObject newBeat = Instantiate(GameManager.Instance.Player1.GetComponent<PlayerScript>().button[1], GameManager.Instance.Player1.GetComponent<PlayerScript>().buttonStart[1].transform.position, GameManager.Instance.Player1.GetComponent<PlayerScript>().buttonStart[1].transform.rotation) as GameObject;
                    newBeat.transform.SetParent(this.gameObject.transform);
                    newBeat.transform.localPosition = new Vector3(-0.33f, 0, 0);
                    GameManager.Instance.player1HitBeats++;
                    GameManager.Instance.player1Score += 5;
                    GameManager.Instance.Player1SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1Sprites[1];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.S))
                {
                    GameObject newBeat = Instantiate(GameManager.Instance.Player1.GetComponent<PlayerScript>().button[2], GameManager.Instance.Player1.GetComponent<PlayerScript>().buttonStart[2].transform.position, GameManager.Instance.Player1.GetComponent<PlayerScript>().buttonStart[2].transform.rotation) as GameObject;
                    newBeat.transform.SetParent(this.gameObject.transform);
                    newBeat.transform.localPosition = new Vector3(0.33f, 0, 0);
                    GameManager.Instance.player1HitBeats++;
                    GameManager.Instance.player1Score += 5;
                    GameManager.Instance.Player1SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1Sprites[2];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.D))
                {
                    GameObject newBeat = Instantiate(GameManager.Instance.Player1.GetComponent<PlayerScript>().button[3], GameManager.Instance.Player1.GetComponent<PlayerScript>().buttonStart[3].transform.position, GameManager.Instance.Player1.GetComponent<PlayerScript>().buttonStart[3].transform.rotation) as GameObject;
                    newBeat.transform.SetParent(this.gameObject.transform);
                    newBeat.transform.localPosition = new Vector3(1f, 0, 0);
                    GameManager.Instance.player1HitBeats++;
                    GameManager.Instance.player1Score += 5;
                    GameManager.Instance.Player1SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1Sprites[3];
                    return;
                }
            }

            if (enterTrigger2 && this.gameObject.transform.childCount == 1)
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow) && this.transform.GetChild(0).CompareTag ("Button0"))
                {
                    GameManager.Instance.player2Score += 5;
                    GameManager.Instance.player2HitBeats++;
                    Destroy(this.transform.GetChild(0).gameObject);
                    GameManager.Instance.Player2SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2Sprites[0];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.UpArrow) && this.transform.GetChild(0).CompareTag("Button1")) //This is not an else so that it can record more than one hit
                {
                    GameManager.Instance.player2Score += 5;
                    GameManager.Instance.player2HitBeats++;
                    Destroy(this.transform.GetChild(0).gameObject);
                    GameManager.Instance.Player2SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2Sprites[1];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.DownArrow) && this.transform.GetChild(0).CompareTag("Button2"))
                {
                    GameManager.Instance.player2Score += 5;
                    GameManager.Instance.player2HitBeats++;
                    Destroy(this.transform.GetChild(0).gameObject);
                    GameManager.Instance.Player2SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2Sprites[2];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.RightArrow) && this.transform.GetChild(0).CompareTag("Button3"))
                {
                    GameManager.Instance.player2Score += 5;
                    GameManager.Instance.player2HitBeats++;
                    Destroy(this.transform.GetChild(0).gameObject);
                    GameManager.Instance.Player2SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2Sprites[3];
                    return;
                }
            }
        }

        else if (GameManager.Instance.upFalseDownTrue)
        {
            if (enterTrigger2 && this.gameObject.transform.childCount < 1)
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    GameObject newBeat = Instantiate(GameManager.Instance.Player2.GetComponent<PlayerScript>().button[0], GameManager.Instance.Player2.GetComponent<PlayerScript>().buttonStart[0].transform.position, GameManager.Instance.Player2.GetComponent<PlayerScript>().buttonStart[0].transform.rotation) as GameObject;
                    newBeat.transform.SetParent(this.gameObject.transform);
                    newBeat.transform.localPosition = new Vector3(-1f, 0, 0);
                    GameManager.Instance.player2HitBeats++;
                    GameManager.Instance.player2Score += 5;
                    GameManager.Instance.Player2SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2Sprites[0];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.UpArrow)) //This is not an else so that it can record more than one hit
                {
                    GameObject newBeat = Instantiate(GameManager.Instance.Player2.GetComponent<PlayerScript>().button[1], GameManager.Instance.Player2.GetComponent<PlayerScript>().buttonStart[1].transform.position, GameManager.Instance.Player2.GetComponent<PlayerScript>().buttonStart[1].transform.rotation) as GameObject;
                    newBeat.transform.SetParent(this.gameObject.transform);
                    newBeat.transform.localPosition = new Vector3(-0.33f, 0, 0);
                    GameManager.Instance.player2HitBeats++;
                    GameManager.Instance.player2Score += 5;
                    GameManager.Instance.Player2SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2Sprites[1];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    GameObject newBeat = Instantiate(GameManager.Instance.Player2.GetComponent<PlayerScript>().button[2], GameManager.Instance.Player2.GetComponent<PlayerScript>().buttonStart[2].transform.position, GameManager.Instance.Player2.GetComponent<PlayerScript>().buttonStart[2].transform.rotation) as GameObject;
                    newBeat.transform.SetParent(this.gameObject.transform);
                    newBeat.transform.localPosition = new Vector3(0.33f, 0, 0);
                    GameManager.Instance.player2HitBeats++;
                    GameManager.Instance.player2Score += 5;
                    GameManager.Instance.Player2SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2Sprites[2];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    GameObject newBeat = Instantiate(GameManager.Instance.Player2.GetComponent<PlayerScript>().button[3], GameManager.Instance.Player2.GetComponent<PlayerScript>().buttonStart[3].transform.position, GameManager.Instance.Player2.GetComponent<PlayerScript>().buttonStart[3].transform.rotation) as GameObject;
                    newBeat.transform.SetParent(this.gameObject.transform);
                    newBeat.transform.localPosition = new Vector3(1f, 0, 0);
                    GameManager.Instance.player2HitBeats++;
                    GameManager.Instance.player2Score += 5;
                    GameManager.Instance.Player2SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player2Sprites[3];
                    return;
                }
            }

            if (enterTrigger1 && this.gameObject.transform.childCount == 1)
            {
                if (Input.GetKeyDown(KeyCode.A) && this.transform.GetChild(0).CompareTag("Button0"))
                {
                    GameManager.Instance.player1Score += 5;
                    GameManager.Instance.player1HitBeats++;
                    Destroy(this.transform.GetChild(0).gameObject);
                    GameManager.Instance.Player1SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1Sprites[0];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.W) && this.transform.GetChild(0).CompareTag("Button1")) //This is not an else so that it can record more than one hit
                {
                    GameManager.Instance.player1Score += 5;
                    GameManager.Instance.player1HitBeats++;
                    Destroy(this.transform.GetChild(0).gameObject);
                    GameManager.Instance.Player1SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1Sprites[1];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.S) && this.transform.GetChild(0).CompareTag("Button2"))
                {
                    GameManager.Instance.player1Score += 5;
                    GameManager.Instance.player1HitBeats++;
                    Destroy(this.transform.GetChild(0).gameObject);
                    GameManager.Instance.Player1SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1Sprites[2];
                    return;
                }
                if (Input.GetKeyDown(KeyCode.D) && this.transform.GetChild(0).CompareTag("Button3"))
                {
                    GameManager.Instance.player1Score += 5;
                    GameManager.Instance.player1HitBeats++;
                    Destroy(this.transform.GetChild(0).gameObject);
                    GameManager.Instance.Player1SpriteGUI.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.player1Sprites[3];
                    return;
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D _collider)
    {
		if (beatStyle == BeatStyle.Mandatory) {
			if (_collider.CompareTag("Player1Start"))
			{
				enterTrigger1 = true;            
			}
			if (_collider.CompareTag("Player2Start"))
			{
				enterTrigger2 = true;
			}
		}
    }


    void OnTriggerExit2D(Collider2D _collider)
    {
		if (beatStyle == BeatStyle.Mandatory) {
			if (_collider.CompareTag("Player1Start"))
			{
				enterTrigger1 = false;
			}
			if (_collider.CompareTag("Player2Start"))
			{
				enterTrigger2 = false;
			}
		}
    }
}
