﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SunDisplay : MonoBehaviour, SongManager.SongNotification {
    public float songProggress;
    public float score;
    public float _songDuration;
    public float sunPosX;
    public float sunPosY;
    public GameObject sun;
    public GameObject bottomLeftMarker;
    public GameObject topRightMarker;

    public Vector3 bottomLeftPoint;
    public Vector3 topRightPoint;
	public float normalisedScore;
    public int rnd;
    public float timeToChangeSun = 5;

    public Sprite[] sunFaces;

	public bool sunCanUpdate = false;

    private int switchCheck = 0;
	 
    // Use this for initialization
    void Start() {

        SongManager.RegisterSongNotification(this);
        bottomLeftPoint = bottomLeftMarker.transform.position;
        topRightPoint = topRightMarker.transform.position;

		sunPosX = Mathf.Lerp(bottomLeftPoint.x, topRightPoint.x, 0.5f);
		sunPosY = Mathf.Lerp(bottomLeftPoint.y, topRightPoint.y, 0.95f);


		sun.transform.position = new Vector2(sunPosX, sunPosY);
    }

    // Update is called once pe rframe
    void Update() {
		if (sunCanUpdate) {
			songProggress += Time.deltaTime;
			score = GameManager.Instance.player1Score + -GameManager.Instance.player2Score;
			normalisedScore = (score - -100.0f) /(100.0f + 100.0f);
            if (switchCheck < GameManager.Instance.switchingWay)
            {
                score = 0;
                switchCheck = GameManager.Instance.switchingWay;
            }
			sunPosX = Mathf.Lerp(bottomLeftPoint.x, topRightPoint.x, normalisedScore);
			sunPosY = Mathf.Lerp(bottomLeftPoint.y, topRightPoint.y, songProggress / _songDuration);


			sun.transform.position = new Vector2(sunPosX, sunPosY);
		}

        rnd = Random.Range(1, 15);

        timeToChangeSun = timeToChangeSun - Time.deltaTime;
        if (timeToChangeSun == 0 || timeToChangeSun < 0)
        {
            timeToChangeSun = 5;
            sun.GetComponent<SpriteRenderer>().sprite = sunFaces[rnd];
        }
    }
    void changeFaces()
    {
        
    }

    void sunPos()
    {

    }
    
	public void OnCountingDownToFirstSong(float timeUntilStart) {
	}

	public void OnBeginSong(float songDuration) {
		_songDuration = songDuration;
		songProggress = 0;
		sunCanUpdate = true;
	}

	public void OnEndSong(float timeUntilNextSongStarts) {
		sunCanUpdate = false;
	}

	public void OnSongBeat(int beatIndex, double beatTime, BeatStyle beatStyle) {
	}

	public void OnSwitchPlayer() {
	}
}
