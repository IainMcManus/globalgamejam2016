﻿using UnityEngine;
using System.Collections;

public class DialogArea : MonoBehaviour {
	public SpriteRenderer DialogSpriteRenderer;
	public Sprite SunSprite;
	public Sprite JoshSprite;
	public Sprite IainSprite;
	public TextMesh DialogText;

	private float fadeOutCountdown;

	[Multiline]
	public string[] StateText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (fadeOutCountdown > 0) {
			fadeOutCountdown -= Time.deltaTime;

			if (fadeOutCountdown < 0) {
				DialogText.color = Color.clear;
				DialogSpriteRenderer.enabled = false;
			}
		}
	}

	public void UpdateDialog(GameManagerState currentState) {
		DialogSpriteRenderer.enabled = true;
		DialogText.color = Color.black;
		DialogText.text = StateText[(int) currentState];
		fadeOutCountdown = 10.0f;

		switch (currentState) {
		case GameManagerState.Intro_Iain:
		case GameManagerState.Response_Iain:
		case GameManagerState.Challenge_Iain:
			DialogSpriteRenderer.sprite = IainSprite;
			break;

		case GameManagerState.Intro_Josh:
		case GameManagerState.Response_Josh:
		case GameManagerState.Challenge_Josh:
			DialogSpriteRenderer.sprite = JoshSprite;
			break;

		case GameManagerState.Intro_Sun:
            case GameManagerState.Josh_Wins:
            case GameManagerState.Iain_Wins:
            case GameManagerState.Both_Win:
                DialogSpriteRenderer.sprite = SunSprite;
			break;
		}
	}
}
