﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Xml.Serialization;
using System.IO;

[CustomEditor(typeof(Song))]
public class SongEditor : Editor {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();

		serializedObject.Update();

		// Retrieve the object being edited
		Song song = (Song)target;

		if (song.Data != null) {
			EditorGUILayout.LabelField("BPM: " + song.Data.AverageBPM.ToString());
			EditorGUILayout.LabelField("Length: " + song.Data.SongLength.ToString());
		}

		if (GUILayout.Button("Import Data")) {
			// Setup XmlSerializer and associate with the menu list's type
			XmlSerializer serializer = new XmlSerializer(typeof(SongData));

			try {
				// Create a stream from the text data
				MemoryStream memStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(song.XMLData.text));

				// Setup a stream reader to read from the xml asset
				using (System.IO.StreamReader reader = new System.IO.StreamReader(memStream)) {
					song.Data = serializer.Deserialize(reader) as SongData;
				}
			}
			catch(System.Exception ex) {
			}	
		}

		serializedObject.ApplyModifiedProperties();
	}
}