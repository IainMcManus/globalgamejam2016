﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SongManagerState {
	Idle,
	InSong,
	SwitchingSong
}

public class SongManager : MonoBehaviour {
	public List<Song> Songs = new List<Song>();
	public int CurrentSong = -1;
	public SongManagerState CurrentState = SongManagerState.Idle;

	private AudioSource PlayerAudio;

	public SongData CurrentSongData;
	public float EffectBPM;
	private double AudioStartTime;
	private int BeatIndex = 0;
	public int BeatStep = 1;

	private int BreakIndex = 0;

	public float IntervalBetweenSongs = 5.0f;

	public float MinimumTimeBetweenBeats = 0.2f;
	public float MinimumPlayerTime = 40.0f;
	public float LeadLagTimeForSwitching = 3.0f;
	public float SwitchPlayerTime = 14.0f;

	private List<SongNotification> SongObservers = new List<SongNotification>();

	public static SongManager Instance;

	public bool SwitchSongOnBreak = true;

	public interface SongNotification
	{
		void OnCountingDownToFirstSong(float timeUntilStart);
		void OnBeginSong(float songDuration);
		void OnEndSong(float timeUntilNextSongStarts);
		void OnSongBeat(int beatIndex, double beatTime, BeatStyle beatStyle);
		void OnSwitchPlayer();
	}

	void Awake() {
		Instance = this;
	}

	public static void RegisterSongNotification(SongNotification notification) {
		Instance.SongObservers.Add(notification);
	}

	public static void DeregisterSongNotification(SongNotification notification) {
		Instance.SongObservers.Remove(notification);
	}

	// Use this for initialization
	void Start () {
		PlayerAudio = GetComponent<AudioSource>();

		foreach(Song song in Songs) {
			song.RunPostProcess(IntervalBetweenSongs, MinimumTimeBetweenBeats, MinimumPlayerTime, LeadLagTimeForSwitching);
		}
	}

	public void StartSongManager() {
		foreach(SongNotification notification in SongObservers) {
			notification.OnCountingDownToFirstSong(IntervalBetweenSongs);
		}

		Invoke("StartNextSong", IntervalBetweenSongs);
	}
	
	// Update is called once per frame
	void Update () {
		if (CurrentState == SongManagerState.SwitchingSong) {
			// Fade out during the lead lag time
			PlayerAudio.volume -= Time.deltaTime / IntervalBetweenSongs;

			if (PlayerAudio.volume <= 0) {
				CurrentState = SongManagerState.Idle;
				PlayerAudio.Stop();

				foreach(SongNotification notification in SongObservers) {
					notification.OnSwitchPlayer();
				}

				Invoke("StartNextSong", SwitchPlayerTime - LeadLagTimeForSwitching);
			}
		} // Are we currently playing?
		else if (PlayerAudio.isPlaying) {
			// Are we in the idle state at the moment
			if (CurrentState == SongManagerState.Idle) {
				AudioStartTime = AudioSettings.dspTime;

				CurrentState = SongManagerState.InSong;

				BeatIndex = 0;

				OnBeginSong(CurrentSongData.SongLength);
			} // running beat detection
			else {
				double elapsedSongTime = AudioSettings.dspTime - AudioStartTime;

				// Check if we need to emit any beat events
				for (int index = BeatIndex; index < CurrentSongData.BeatTimes.Count; index += BeatStep) {
					// Is the time past this beat?
					if (elapsedSongTime >= CurrentSongData.BeatTimes[index]) {
						OnSongBeat(index, CurrentSongData.BeatTimes[index], CurrentSongData.OptionalBeats[index]);
						++BeatIndex;
					} // we know no others will be passed so we can skip after this point
					else {
						break;
					}
				}

				// Have we passed a break
				for (int index = BreakIndex; index < CurrentSongData.BreakTimes.Count; ++index) {
					// have we passed the break?
					if (elapsedSongTime >= CurrentSongData.BreakTimes[index]) {
						BreakIndex = index + 1;

						OnSwitchPlayer();

						break;
					}
				}
			}
		} // not playing check if we are in the in playing state so that we can flip back to idle
		else if (CurrentState == SongManagerState.InSong) {
			CurrentState = SongManagerState.Idle;
			OnEndSong();
		}
	}

	void BeginSong(int songIndex) {
		CurrentSong = songIndex;
		CurrentSongData = Songs[CurrentSong].Data;
		EffectBPM = CurrentSongData.AverageBPM / BeatStep;

		// Intentionally delaying the player here so that can pick up on the start of play
		PlayerAudio.clip = Songs[CurrentSong].AudioFile;
		PlayerAudio.PlayDelayed(0.1f);
	}

	void OnBeginSong(float songDuration) {
		PlayerAudio.volume = 1.0f;

		foreach(SongNotification notification in SongObservers) {
			notification.OnBeginSong(songDuration);
		}
	}

	void OnSwitchPlayer() {
		// Flip to switching the song
		if (SwitchSongOnBreak) {
			CurrentState = SongManagerState.SwitchingSong;
		}
		else {
			foreach(SongNotification notification in SongObservers) {
				notification.OnSwitchPlayer();
			}
		}
	}

	void OnEndSong() {
		foreach(SongNotification notification in SongObservers) {
			notification.OnEndSong(IntervalBetweenSongs);
		}

		foreach(SongNotification notification in SongObservers) {
			notification.OnSwitchPlayer();
		}

		Invoke("StartNextSong", IntervalBetweenSongs);
	}

	void StartNextSong() {
		// Update the current song
		BeginSong((CurrentSong + 1) % Songs.Count);
	}

	void OnSongBeat(int beatIndex, double beatTime, BeatStyle beatStyle) { 
		if (CurrentState == SongManagerState.SwitchingSong) {
			return;
		}

		foreach(SongNotification notification in SongObservers) {
			notification.OnSongBeat(beatIndex, beatTime, beatStyle);
		}
	}
}
