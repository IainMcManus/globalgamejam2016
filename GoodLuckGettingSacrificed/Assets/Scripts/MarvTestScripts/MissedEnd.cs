﻿using UnityEngine;
using System.Collections;

public class MissedEnd : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D _collider)
    {
        if (_collider.transform.childCount > 0)
        {
            if (GameManager.Instance.upFalseDownTrue)
            {
                GameManager.Instance.player1MissedBeats++;
                GameManager.Instance.player1Score -= 5;
            }
            else
            {
                GameManager.Instance.player2MissedBeats++;
                GameManager.Instance.player2Score -= 5;
            } 
        }
    }
}
