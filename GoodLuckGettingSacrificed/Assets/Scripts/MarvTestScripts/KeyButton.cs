﻿using UnityEngine;
using System.Collections;

public class KeyButton : MonoBehaviour
{
    public float lifeTime = 20.0f;

	// Use this for initialization
	void Start ()
    {
        Destroy(this.gameObject, lifeTime);
	}
}
