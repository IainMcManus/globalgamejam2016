﻿using UnityEngine;
using System.Collections;

public enum GameManagerState {
	Intro_Pause,
	Intro_Iain,
	Intro_Josh,
	Intro_Sun,
	Challenge_Iain,
	Response_Josh,
	Iain_vs_Josh,
	Challenge_Josh,
	Response_Iain,
	Josh_vs_Iain,
    Josh_Wins,
    Iain_Wins,
    Both_Win
}

public class GameManager : MonoBehaviour, SongManager.SongNotification
{
    public static GameManager Instance;

	public GameManagerState currentState = GameManagerState.Intro_Pause;

	public float[] StateTimes;
	public float TimeUntilNextStateTransition;
	public AudioClip[] StateAudioClips;
	public AudioSource DialogAudioSource;

    private bool upFalseDownTrue_Internal = false;
    public int switchingWay = 0;

    public int player1HitBeats = 0;
    public int player2HitBeats = 0;
    public int player1TotalKeys = 0;
    public int player2TotalKeys = 0;
    public int player1MissedBeats = 0;
    public int player2MissedBeats = 0;

    public Sprite[] player1ButtonSprites;
    public Sprite[] player2ButtonSprites;
    public Sprite[] player1Sprites;
    public Sprite[] player2Sprites;
    // 0 - A / Left  
    // 1 - W / Up
    // 2 - S / Down
    // 3 - D / Right
    // 4 - Stab
    // 5 - Praise
    // 6 - Praying

    public int player1Score = 0;
    public int player2Score = 0;
    public int player1RoundScore = 0;
    public int player2RoundScore = 0;

    public GameObject Player1;
    public GameObject Player2;
    public GameObject Player1SpriteGUI;
    public GameObject Player2SpriteGUI;

    public GameObject Player1Miss;
    public GameObject Player2Miss;

	public BeatDisplay Player1BeatDisplay;
	public BeatDisplay Player2BeatDisplay;

	public bool upFalseDownTrue
	{
		get {
			return upFalseDownTrue_Internal;
		}
		set {
			upFalseDownTrue_Internal = value;
			Player1BeatDisplay.MovingUp = !upFalseDownTrue_Internal;
			Player2BeatDisplay.MovingUp = upFalseDownTrue_Internal;
		}
	}

	void Awake() {
		Instance = this;

		TimeUntilNextStateTransition = StateTimes[(int)currentState];
		SetDialogBasedOnState();
	}

    // Use this for initialization
    void Start ()
    {
		SongManager.RegisterSongNotification(this);

		upFalseDownTrue = false;
	}

	private bool hasEverStartedSong = false;
	public DialogArea gameDialogArea;

	void SetDialogBasedOnState() {
		// Play the audio clip
		if (StateAudioClips[(int)currentState] != null) {
			DialogAudioSource.clip = StateAudioClips[(int)currentState];
			DialogAudioSource.Play();

			gameDialogArea.UpdateDialog(currentState);
		}

		if (!hasEverStartedSong && (currentState >= GameManagerState.Iain_vs_Josh)) {
			hasEverStartedSong = true;
			SongManager.Instance.StartSongManager();
		}
	}

	private bool checkStateTransition = true;

	// Update is called once per frame
	void Update ()
    {
        if (checkStateTransition) {
			if (currentState < GameManagerState.Iain_vs_Josh) {
				TimeUntilNextStateTransition -= Time.deltaTime;

				if (TimeUntilNextStateTransition <= 0.0f) {
					currentState = (GameManagerState)(currentState + 1);
                    SetDialogBasedOnState();
					TimeUntilNextStateTransition = StateTimes[(int)currentState];
					if (currentState == GameManagerState.Iain_vs_Josh) {
						checkStateTransition = false;
					}
				}

				return;
			}
			else if (currentState < GameManagerState.Josh_vs_Iain) {
				TimeUntilNextStateTransition -= Time.deltaTime;

				if (TimeUntilNextStateTransition <= 0.0f) {
					currentState = (GameManagerState)(currentState + 1);
                    SetDialogBasedOnState();
					TimeUntilNextStateTransition = StateTimes[(int)currentState];

					if (currentState == GameManagerState.Josh_vs_Iain) {
						checkStateTransition = false;
					}
				}

				return;
			}
		}

        if (Input.GetKeyDown(KeyCode.A))
        {
            ++player1TotalKeys;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            ++player1TotalKeys;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            ++player1TotalKeys;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            ++player1TotalKeys;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            ++player2TotalKeys;
        }
        
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ++player2TotalKeys;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            ++player2TotalKeys;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            ++player2TotalKeys;
        }

        if (player1HitBeats < player1TotalKeys)
        {
            player1Score += (player1TotalKeys - player1HitBeats) * -3;
            player1HitBeats = 0;
            player1TotalKeys = 0;
            Player1SpriteGUI.GetComponent<SpriteRenderer>().sprite = player1Sprites[6];
        }

        if (player2HitBeats < player2TotalKeys)
        {
            player2Score += (player2TotalKeys - player2HitBeats) * -3;
            player2HitBeats = 0;
            player2TotalKeys = 0;
            Player2SpriteGUI.GetComponent<SpriteRenderer>().sprite = player2Sprites[6];
        }

        if (switchingWay >= 5)
        {
            EndGame();
        }
    }

	public void OnCountingDownToFirstSong(float timeUntilStart) {
	}

	public void OnBeginSong(float songDuration) {
	}

	public void OnEndSong(float timeUntilNextSongStarts)
    {

	}

	public void OnSongBeat(int beatIndex, double beatTime, BeatStyle beatStyle) {
	}

	public void OnSwitchPlayer() {
		if (currentState == GameManagerState.Iain_vs_Josh) {
			currentState = GameManagerState.Challenge_Josh;
            TimeUntilNextStateTransition = StateTimes[(int)currentState];
			SetDialogBasedOnState();
			checkStateTransition = true;
		}
		else {
			currentState = GameManagerState.Challenge_Iain;
            TimeUntilNextStateTransition = StateTimes[(int)currentState];
			SetDialogBasedOnState();
			checkStateTransition = true;
		}

        upFalseDownTrue = !upFalseDownTrue;

        if(player1Score > player2Score)
        {
            player1RoundScore++;
            player1Score = 0;
            player2Score = 0;
        }
        else if (player1Score < player2Score)
        {
            player2RoundScore++;
            player1Score = 0;
            player2Score = 0;
        }
        else
        {
            player1RoundScore++;
            player2RoundScore++;
            player1Score = 0;
            player2Score = 0;
        }
        switchingWay++;
    }

    public void EndGame()
    {
        Destroy(SongManager.Instance.gameObject);

        if (player1RoundScore > player2RoundScore)
        {
            currentState = GameManagerState.Iain_Wins;
            SetDialogBasedOnState();
            Invoke("ReturnToMainMenu", 7.0f);
        }
        else if (player1RoundScore < player2RoundScore)
        {
            currentState = GameManagerState.Josh_Wins;
            SetDialogBasedOnState();
            Invoke("ReturnToMainMenu", 7.0f);
        }
        else
        {
            currentState = GameManagerState.Both_Win;
            SetDialogBasedOnState();
            Invoke("ReturnToMainMenu", 7.0f);
        }
    }

    void ReturnToMainMenu()
    {
        Application.LoadLevel("Pause");
    }
}
