﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

public enum BeatStyle {
	Mandatory,
	Optional_Red,
	Optional_Orange,
	Optional_Green
}

[System.Serializable]
public class SongData {
	public List<double> BeatTimes = new List<double>();
	public string SongName = "Unknown";
	public float SongLength;
	public int AverageBPM;

	[System.NonSerialized]
	public List<double> BreakTimes = new List<double>();

	[System.NonSerialized]
	public List<BeatStyle> OptionalBeats = new List<BeatStyle>();

	[System.NonSerialized]
	public List<double> BlockBeginTimes = new List<double>();

	public SongData() {
	}
}

public class Song : MonoBehaviour {
	public SongData Data;
	public AudioClip AudioFile;
	public TextAsset XMLData;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void PostProcess_BeatFilter(double minimumBeatInterval) {
		double lastTime = Data.BeatTimes[0];

		// filter out beats that are too close
		for (int index = 1; index < Data.BeatTimes.Count; ++index) {
			double beatTime = Data.BeatTimes[index];

			// if the beat is too close then remove it
			if ((beatTime - lastTime) <= minimumBeatInterval) {
				Data.BeatTimes.RemoveAt(index);
				--index;
				continue;
			}

			lastTime = beatTime;
		}
	}

	void PostProcess_AddBreaks(double playTimeBeforeSwitch, double switchLeadLagTime) {
		double timePerPlayer = playTimeBeforeSwitch + (switchLeadLagTime * 1);

		// work out the number of blocks
		int currentNumBlocks = Mathf.CeilToInt((float)(Data.SongLength - switchLeadLagTime) / (float)timePerPlayer);

		// round to the nearest even number below
		currentNumBlocks = currentNumBlocks - (currentNumBlocks % 2);

		// actual per player time
		double actualTimePerPlayer = Data.SongLength / currentNumBlocks;

		// Process the block data
		for (int block = 0; block < (currentNumBlocks - 1); ++block) {
			double breakStartTime = switchLeadLagTime + actualTimePerPlayer * (block + 1);
			double breakEndTime = breakStartTime + (switchLeadLagTime * 1);

			Data.BreakTimes.Add(breakStartTime);

			// filter out any beats in the break
			for (int index = 0; index < Data.BeatTimes.Count; ++index) {
				double beatTime = Data.BeatTimes[index];

				// if the beat is too close then remove it
				if ((beatTime >= breakStartTime) && (beatTime <= breakEndTime)) {
					Data.BeatTimes.RemoveAt(index);
					--index;
					continue;
				}
			}
		}
	}

	public void RunPostProcess(double songLeadTime, double minimumBeatInterval, double playTimeBeforeSwitch, double switchLeadLagTime) {
		PostProcess_BeatFilter(minimumBeatInterval);

		PostProcess_AddBreaks(playTimeBeforeSwitch, switchLeadLagTime);

		// Clear all of the optional beats
		Data.OptionalBeats.Clear();

		double beatBeginTime = switchLeadLagTime;
		double blockEndTime = 0.0;
		int optionalBeatsFound = 0;
		int breakIndex = 0;
		for (int index = 0; index < Data.BeatTimes.Count; ++index) {
			// grab the time of the beat
			double beatTime = Data.BeatTimes[index];

			// if the beat is within the time window then make it optional
			if (beatTime <= blockEndTime) {
				Data.OptionalBeats.Add(BeatStyle.Mandatory);
			}
			else if (beatTime <= beatBeginTime) {
				++optionalBeatsFound;
				Data.OptionalBeats.Add(BeatStyle.Optional_Green);
			}
			else {
				Data.OptionalBeats.Add(BeatStyle.Mandatory);

				// Determine the number of beats per colour change
				int beatColourStep = Mathf.Max(optionalBeatsFound / 3, 1);
				BeatStyle currentStyle = BeatStyle.Optional_Green;

				for (int beatIndex = 0; beatIndex <= optionalBeatsFound; ++beatIndex) {
					Data.OptionalBeats[index - beatIndex] = currentStyle;

					currentStyle = BeatStyle.Optional_Green + ((3 - ((beatIndex + 1) / beatColourStep)) % 3);
				}

				optionalBeatsFound = 0;
				beatBeginTime = Data.BreakTimes[breakIndex] + (switchLeadLagTime * 2);
				blockEndTime = Data.BreakTimes[breakIndex];

				++breakIndex;

				if (breakIndex >= Data.BreakTimes.Count) {
					break;
				}
			}
		}
	}
}
